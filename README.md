# Task 7 - File System Manager

## Description
A Java console application that can be used to manage and manipulate files. The funtionalities of the system are the followings:
- List of file names in a given diretory
- List of specific files by their extension
- Manipulate provided .txt

## Usage
1. Clone the git repository.
    - git clone git@gitlab.com:jpark0624/task-7-file-system-manager.git
2. Compile the application.
3. Redirect to out folder.
4. Creat JAR file.
5. Run the created JAR file.

![screenshot](screenshot/task7.PNG)