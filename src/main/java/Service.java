import java.util.Scanner;
import java.io.*;

public class Service {
    Scanner input = new Scanner(System.in);
    Logger logger = new Logger();

    public void close() {
        input.close();
        logger.endLogging();
        System.exit(1);
    }

    public void allFiles() {
        //Names of all files
        String[] children = new File("../src/main/resources").list();
        if (children == null) {
            System.out.println("The directory is empty.");
        } else {
            for (String filename : children) {
                System.out.println(filename);
            }
        }
        System.out.println();
    }

    public String extFiles() {
        //All files with same extension
        System.out.print("Which extension do you want to list? ");
        String ext = input.nextLine();
        String[] children = new File("../src/main/resources").list();
        boolean any = true;
        while (ext.equals("")) {
            System.out.println("No extension given!");
            System.out.println();
            ext = extFiles();
        }
        if (children == null) {
            System.out.println("The directory is empty.");
        } else {
            for (String name : children) {
                String[] parted = name.split("\\.(?=[^\\.]+$)");
                if (parted[1].equals(ext)) {
                    System.out.println(name);
                    any = false;
                }
            }
            if (any) {
                System.out.println("No files had that extension.");
            }
        }
        return ext;
    }

    public void getFileName(File filename) {
        long time = System.currentTimeMillis();
        System.out.println("The name of the file is " + filename.getName());
        System.out.println();
        time = System.currentTimeMillis() - time;
        logger.logging("Got the name of the file " + filename.getName()
                        + ". The function took " + time + "ms to execute.");
    }

    public void getFileSize(File filename) {
        long time = System.currentTimeMillis();
        System.out.println("The filesize is " + filename.length() + " bytes");
        System.out.println();
        time = System.currentTimeMillis() - time;
        logger.logging("Got the size of " + filename.length()
                        + " bytes, of the file " + filename.getName()
                        + ". The function took " + time + "ms to execute.");
    }

    public void lineAmount(File filename) {
        long time = System.currentTimeMillis();
        int lines = 0;
        try {
            Scanner readFile = new Scanner(filename);
            while (readFile.hasNextLine()) {
                lines++;
                readFile.nextLine();
            }
            System.out.println("The file has " + lines + " lines.");
            readFile.close();
        } catch(Exception e) {
            System.out.println("Error: Could not read file.");
        }
        System.out.println();

        time = System.currentTimeMillis() - time;
        logger.logging("Got the amount of lines, " + lines + ", of the file "
                        + filename.getName() + ". The function took " + time
                        + "ms to execute.");
    }

    public void wordSearch(File filename) {
        long time = System.currentTimeMillis();
        System.out.print("Which word do you want to search for? ");
        String wordSearched = input.nextLine().toLowerCase();
        boolean inFile = false;
        int lines = 0;
        String inFilePrint = "";
        //String word;
        try {
            Scanner readFile = new Scanner(filename);
            
            while (readFile.hasNextLine()) {
                String line = readFile.nextLine();
                lines++;
                if (line.toLowerCase().contains(wordSearched)) {
                    System.out.println("The word appears first on line "
                                        + lines + ".");
                    inFile = true;
                    inFilePrint = ", which is in the file. ";
                    break;
                }
            }
            if (!inFile) {
                System.out.println("The word is not in the file.");
                inFilePrint = ", which is not in the file. ";
            }
            readFile.close();
        } catch(Exception e) {
            System.out.println("Error: Could not read file.");
        }
        System.out.println();
        
        time = System.currentTimeMillis() - time;
        logger.logging("Searched for the word " + wordSearched + inFilePrint
                        + "The function took " + time + "ms to execute.");
    }

    public void wordUsage(File filename) {
        long time = System.currentTimeMillis();
        System.out.print("Which word do you want to search for? ");
        String wordSearched = input.nextLine().toLowerCase();
        boolean inFile = false;
        int amount = 0;
        String inFilePrint = "";
        String word;
        try {
            Scanner readFile = new Scanner(filename);
            while (readFile.hasNext()) {
                word = readFile.next().toLowerCase();
                if (word.contains(wordSearched)) {
                    amount++;
                    inFile = true;
                }
            }
            if (!inFile) {
                System.out.println("The word is not in the file.");
                inFilePrint = ", which is not in the file. ";
            } else {
                System.out.println("The word is used " + amount
                                    + " times in the file.");
                inFilePrint = ", which was found " + amount + " times. ";
            }
            readFile.close();
        } catch(Exception e) {
            System.out.println("Error: Could not read file.");
        }
        System.out.println();

        time = System.currentTimeMillis() - time;
        logger.logging("Searched for the word " + wordSearched + inFilePrint
                        + "The function took " + time + "ms to execute.");
    }
}