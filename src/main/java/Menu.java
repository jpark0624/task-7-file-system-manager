import java.util.Scanner;
import java.io.*;

public class Menu {
    Service fileService = new Service();
    Scanner input = new Scanner(System.in);
    
    public void displayMenu() {
        System.out.println();
        System.out.println("############ Menu #############");
        System.out.println("1. List all files in directory");
        System.out.println("2. List all files with same extension");
        System.out.println("3. Get information about a file");
        System.out.println("q. Quit");
        System.out.println("###############################");
        select();
    }
    
    public void select() {
        String number = ask();
        switch (number) {
            case "1":
                fileService.allFiles();
                select();
                break;
            case "2":
                fileService.extFiles();
                select();
                break;
            case "3":
                selectFile();
                break;
            case "q":
                close();
                break;
            default:
                System.out.println("select a valid option!");
                select();
                break;
        }
    }
    
    public String ask() {
        System.out.print("Select an option: ");
        String number = "";
        try {
            number = input.next();
        } catch(Exception e) {
            System.out.println("Invalid option. Please select between 1 - 3");
        }
        return number;
    }

    public void close() {
        input.close();
        System.exit(1);
    }

    public String selectFile() {
        System.out.print("Which file? ");
        String name = "";
        try {
            name = "../src/main/resources/" + input.next();
        } catch(Exception e) {
            System.out.println("Could not find the file.");
        }
        File filename = new File(name);
        while (name.equals("") || !(filename.exists())) {
            System.out.println("No valid filename given.");
            System.out.println();
            name = selectFile();
        }
        fileMenu(filename);
        return name;
    }

    public void fileMenu(File filename) {
        System.out.println();
        System.out.println("1. Display filemanipulation menu");
        System.out.println("2. Get filename");
        System.out.println("3. Get size of file");
        System.out.println("4. Amount of lines in file");
        System.out.println("5. Search for a word");
        System.out.println("6. Search for how often a word is used");
        System.out.println("7. select a different file");
        System.out.println("9. Back to main menu");
        System.out.println("q. Quit");
        selectFileOption(filename);
    }

    public void selectFileOption(File filename) {
        String number = ask();
        switch (number) {
            case "1":
                fileMenu(filename);
                break;
            case "2": 
                fileService.getFileName(filename);
                selectFileOption(filename);
                break;
            case "3":
                fileService.getFileSize(filename);
                selectFileOption(filename);
                break;
            case "4":
                fileService.lineAmount(filename);
                selectFileOption(filename);
                break;
            case "5":
                fileService.wordSearch(filename);
                selectFileOption(filename);
                break;
            case "6":
                fileService.wordUsage(filename);
                selectFileOption(filename);
                break;
            case "7":
                selectFile();
            case "9":
                displayMenu();
            case "q":
                fileService.close();
            default:
                System.out.println("Not a valid option!");
                selectFileOption(filename);
                break;
        }
    }
}
